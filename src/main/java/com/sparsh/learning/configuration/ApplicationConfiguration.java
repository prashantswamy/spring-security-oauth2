package com.sparsh.learning.configuration;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

// http://websystique.com/spring-security/secure-spring-rest-api-using-oauth2/

// Use the swagger file http://localhost:8080/SpringSecurityOAuth2/swagger-ui.html

@Configuration
@EnableSwagger2
@EnableWebMvc
@ComponentScan(basePackages = "com.sparsh.learning")
public class ApplicationConfiguration implements WebMvcConfigurer {

	// Below 2 methods are for Swagger
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2) //
				.select() //
				.apis(RequestHandlerSelectors.basePackage("com.sparsh.learning")) // .apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.any()) //
				.build() //
				.apiInfo(getApiInfo());

	}

	@SuppressWarnings("rawtypes")
	private ApiInfo getApiInfo() {
		String title = "Spring Security Rest Application";
		String description = "Spring Security Rest Application with Swagger";
		String version = "1.0";
		String termsOfServiceUrl = "Terms of Serice";
		Contact contact = new Contact("Prashant Swamy", "https://twitter.com/prashantswamy",
				"swamy.prashant@gmail.com");
		String license = "Apache License Version 2.0";
		String licenseUrl = "https://www.apache.org/licesen.html";
		Collection<VendorExtension> vendorExtensions = new ArrayList<VendorExtension>();
		// VendorExtension vendorExtension = new StringVendorExtension("VendorKey", "VendorValue");
		// vendorExtensions.add(vendorExtension);
		return new ApiInfo(title, description, version, termsOfServiceUrl, contact, license, licenseUrl,
				vendorExtensions);
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		// enabling swagger-ui part for visual documentation
		registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}

}