package com.sparsh.learning.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.approval.UserApprovalHandler;
import org.springframework.security.oauth2.provider.token.TokenStore;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

	private static String REALM = "MY_OAUTH_REALM";

	@Autowired
	private TokenStore tokenStore;

	@Autowired
	private UserApprovalHandler userApprovalHandler;

	@Autowired
	@Qualifier("authenticationManagerBean")
	private AuthenticationManager authenticationManager;

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {

		PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

		clients.inMemory() //
				.withClient("my-trusted-client") // This is to be passed as username while requesting for access token
				.secret(encoder.encode("secret")) // This is to be passed as password while requesting for access token
				.authorizedGrantTypes("password", "authorization_code", "refresh_token", "implicit") // These are most common OAuth 2.0 grant types
				.authorities("ROLE_CLIENT", "ROLE_TRUSTED_CLIENT") // TODO
				.scopes("read", "write", "trust") //
				.accessTokenValiditySeconds(120) // Access token is only valid for 2 minutes.
				.refreshTokenValiditySeconds(600); // Refresh token is only valid for 10 minutes.

		// other OAuth 2.0 grant types are Client Credentials (client_credentials), Device Code (urn:ietf:params:oauth:grant-type:device_code)

		// #Scope is a mechanism in OAuth 2.0 to limit an application's access to a user's account. An application can request one or more scopes,
		// this information is then presented to the user in the consent screen, and the access token issued to the application will be limited to
		// the scopes granted.

		// #authorizedGrantTypes - Oauth2 provides below grant types for different use cases.
		// Authorization Code -- for apps running on a web server, browser-based and mobile apps
		// Password -- for logging in with a username and password
		// Client credentials -- for application access
		// Implicit -- was previously recommended for clients without a secret, but has been superseded by using the Authorization Code grant with
		// no secret.

		// Authorities are permissions associated with the OAuth Client when the application is acting on its own behalf and there is no user
		// involvement. This is associated with the grant_type of Client_Credentials. The scenario is Application or API trying to access a resource
		// with its own credentials without user involvement.
	}

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints.tokenStore(tokenStore) //
				.userApprovalHandler(userApprovalHandler) //
				.authenticationManager(authenticationManager);
	}

	@Override
	public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
		oauthServer.realm(REALM + "/client");
	}
}